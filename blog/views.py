# coding=utf-8
from django.shortcuts import render
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login
from django.views.generic.edit import FormView
from django.http import HttpResponseRedirect
from django.views.generic.base import View
from django.contrib.auth import logout
from .models import Msg, Subscriber, User
from django.forms import ModelForm
from django.forms import Textarea, TextInput
from django.shortcuts import redirect
from django.contrib.auth.forms import UserCreationForm


class RegisterFormView(FormView):
    form_class = UserCreationForm

    # Ссылка, на которую будет перенаправляться пользователь в случае успешной регистрации.
    # В данном случае указана ссылка на страницу входа для зарегистрированных пользователей.
    success_url = "blog/login/"

    # Шаблон, который будет использоваться при отображении представления.
    template_name = "blog/register.html"

    def form_valid(self, form):
        # Создаём пользователя, если данные в форму были введены корректно.
        form.save()

        # Вызываем метод базового класса
        return super(RegisterFormView, self).form_valid(form)


class LoginFormView(FormView):
    form_class = AuthenticationForm

    # Аналогично регистрации, только используем шаблон аутентификации.
    template_name = "blog/login.html"

    # В случае успеха перенаправим на главную.
    success_url = "blog/"

    def form_valid(self, form):
        # Получаем объект пользователя на основе введённых в форму данных.
        self.user = form.get_user()

        # Выполняем аутентификацию пользователя.
        login(self.request, self.user)
        return super(LoginFormView, self).form_valid(form)


class LogoutView(View):
    def get(self, request):
        # Выполняем выход для пользователя, запросившего данное представление.
        logout(request)

        # После чего, перенаправляем пользователя на главную страницу.
        return HttpResponseRedirect("/")


class BlogForm(ModelForm):
    class Meta:
        model = Msg
        fields = ['title', 'text']
        widgets = {
            'title': TextInput(attrs={'placeholder': 'Заголовок', 'class': 'col-md-12 form-control'}),
            'text': Textarea(attrs={'placeholder': 'Текст', 'class': 'col-md-12 form-control'})}


def index(request):
    # Проверка на авторизацию
    if request.user.is_authenticated():
        context = {'form': BlogForm()}
        return render(request, 'blog/index.html', context)
    else:
        return redirect('login/')


def add_msg(request):
    if request.method == 'POST':
        form = BlogForm(request.POST)
        form.user_id = request.user.id
        if form.is_valid():
            msg = form.save()
            return True
        else:
            return False


def content(request):
    if request.method == 'GET':
        context = None
        temp = ''
        var = request.GET['type_content']
        if var == u'users':
            context = {'users': User.objects.all()}
            temp = 'blog/users.html'
        elif var == u'tape':
            context = {'subscribe': Subscriber.objects.filter(user_id=request.user.id)}
            temp = 'blog/tape.html'
        elif var == u'self':
            context = {'msg': Msg.objects.filter(user_id=request.user.id)}
            temp = 'blog/blog.html'
        return render(request, temp, context)
