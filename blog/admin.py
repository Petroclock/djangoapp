from django.contrib import admin
from .models import Msg
from .models import Subscriber

admin.site.register(Msg)
admin.site.register(Subscriber)
