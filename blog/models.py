# coding=utf-8
from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import User


class Msg(models.Model):
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    title = models.CharField('Заголовок', max_length=50)
    text = models.TextField('Текст', max_length=1000)
    created = models.DateTimeField(auto_now_add=True)


class Subscriber(models.Model):
    user_id = models.ForeignKey(User, on_delete=models.CASCADE, related_name='follower')
    subscriber = models.ForeignKey(User, on_delete=models.CASCADE, related_name='subscriber')
