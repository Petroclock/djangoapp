from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^content/$', views.content, name='content'),
    url(r'^add_msg/$', views.add_msg, name='add_msg'),
    url(r'^login/$', views.LoginFormView.as_view()),
    url(r'^register/$', views.RegisterFormView.as_view()),
]
